$(document).ready(function () {

    $('.more').click(function () {
       $('.acordion-box').toggleClass('active');
       $(this).toggleText('Ver más','Ver menos');

    });

});

$.fn.toggleText = function(t1, t2){
    if (this.text() == t1) this.text(t2);
    else                   this.text(t1);
    return this;
};
let lista;
var cursos;
var clase;
$(document).ready(function(){



    listarDocentes();

    listarModalidad();

    listar();

    $(".filtros select[name=estado]").bind("change",listar);

    $( ".datepicker" ).datepicker();

    $('.btn-agregar').click(function(e){
        e.preventDefault();
        $("#modalagregar").modal("show");
    });

    $("#cursos .lista" ).sortable({
        items: "> tr",
        opacity: 0.7,
        update:function(event,ui){

            $(".orden").show();

        }
    });

    $("#cursos select[name=estado]").bind("change",function(){
        var es = $(this).val();

        switch (es) {
            case "1":
                $(".lista tr[data-estado='0']").hide();
                $(".lista tr[data-estado='1']").show();
                break;
            case "0":
                $(".lista tr[data-estado='1']").hide();
                $(".lista tr[data-estado='0']").show();
                break;
            default:

        }
    });


    $(".orden .btn-guardar").click(function(){
        var ids = $( "#cursos .lista").sortable( "toArray",{attribute:"data-id"} );
        new Request("curso/ordenar",{
            "lista":ids.join(",")
        },function(res){
            listar();

        });
        $("#cursos .orden").hide();

    });


    $("#cursos .orden .btn-cancelar").click(function(){
        $(".orden").hide();
        $("#cursos .lista").empty();
        llenar();
    });

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm:true,
        beforeSubmit:function(){
            $("#cargando").fadeIn(200);
            $("#modalagregar .send").attr("disabled", true);
        },
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modalagregar textarea[name=objetivos]").val($('#modalagregar .objetivos-t .ql-editor').html());
        },
        success: function(res){
            if(res.res!=0){
                $("#modalagregar").modal("hide");
                listar();
            }
        }
    }
    $("#modalagregar form").ajaxForm(options);


    var options = {
        dataType: 'json',
        type: 'post',
        resetForm: true,
        beforeSubmit:function(){
            $("#cargando").fadeIn(200);
            $("#modaleditar .send").attr("disabled", true);
        },
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modaleditar textarea[name=objetivos]").val($('#modaleditar .objetivos-t .ql-editor').html());
        },
        success: function (res) {
            if (res.res == "ok") {
                $("#cargando").fadeOut(200);
                $("#modaleditar").modal("hide");
                $("#modaleditar .send").attr("disabled", false);
                $("#productos .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);



    //Editor
    $('.edit').each(function(index){
        var container = $('.edit').get(index);

        var options = {
            modules: {
                toolbar:  [
                    ['bold', 'italic', 'underline'],
                    [{'list':'ordered'},{'list':'bullet'}],
                    [{ 'align': [] }],
                    ['link', 'image']
                ],
                table: true
            },
            theme: 'snow'  // or 'bubble'
        };
        new Quill(container,options);
    });


});

function listarDocentes(){
    new Request("docente/listarActivos/",null,function(res){
        llenarDocente(res);
    });
}

function listarModalidad(){
    new Request("modalidad/listarmodalidad/",{},function(res){
        console.log(res);
        llenarModalidad(res);
    });
}

function listar(){
    $("#cursos .lista").empty();
    listahtml = Array();

    var es = $(".filtros select[name=estado]").val();
    new Request("curso/listar/",{
        'estado': es
    },function(res){

        lista = res;
        $(".orden").hide();
        llenar();


    });
}



function llenar(){
    $.each(lista,function(k,v){
        var it = new ItemCurso(v);
        $("#cursos .lista").append(it);
    });
}
function llenarDocente(data){
    $.each(data,function(k,v){
        var it = new ItemTrainer(v);
        $("select[name='trainer']").append(it);
    });
}

function llenarModalidad(data){
    $.each(data,function(k,v){
        var it = new ItemModalidad(v);
        $("select[name='modalidad']").append(it);
    });
}


var ItemTrainer = function (data) {
    var html = $('<option value="'+data.trn_id+'">'+data.trn_nombre+'</option>');
    return html;
};

var ItemModalidad = function (data) {
    var html = $('<option value="'+data.mod_id+'">'+data.mod_nombre+'</option>');
    return html;
};



var ItemCurso = function(data){
    //console.log(data);


    var html = $('<tr alias="'+data.cur_alias+'" data-id="'+data.cur_id+'" data-estado="'+data.cur_estado+'">\
		<td>'+data.cur_nombre+'</td>\
		<td>\
			<button class="btn btn-primary btn-editar"> <i class="fas\ fa-edit"></i> Editar</button>\
			<a class="btn btn-success btn-clases" href="./planes/'+data.cur_id+'"> <i class="fas fa-book-open"></i> Plan de estudios</a>\
		</td>\
		<td>\
		    <input type="radio" name="principal">\
		</td>\
	</tr>');


    if (data.cur_principal == "1"){
        html.find('[name=principal]').attr('checked', true);
    }

    html.find("[name='principal']").on('change',function () {

        new Request("curso/principal",{
            "id": data.cur_id
        },function(res){



        },"post");

    });

    html.find(".btn-editar").click(function(){


        $("#modaleditar input[name=id]").val(data.cur_id);
        $("#modaleditar input[name=nombre]").val(data.cur_nombre);
        $("#modaleditar input[name=subtitulo]").val(data.cur_subtitulo);
        $("#modaleditar input[name=short]").val(data.cur_short);
        $("#modaleditar input[name=horario]").val(data.cur_horario);
        $("#modaleditar input[name=fecha]").val(data.cur_fecinicio);
        $("#modaleditar select[name=modalidad]").val(data.cur_modalidad);
        $("#modaleditar select[name=trainer]").val(data.cur_trainer);
        $("#modaleditar textarea[name=descripcion]").val(data.cur_descripcion);
        $("#modaleditar textarea[name=dirigido]").val(data.cur_dirigido);
        $("#modaleditar input[name=duracion]").val(data.cur_duracion_horas);
        $("#modaleditar select[name=estado]").val(data.cur_estado);


        $('#modaleditar .objetivos-t .ql-editor').html(data.cur_objetivos);
        $("#modaleditar textarea[name=objetivos]").val(data.cur_objetivos);

        $("#modaleditar img.foto").attr("src","");
        $("#modaleditar img.foto").attr("src",path+"files/curso/"+data.cur_foto);


        $("#modaleditar img.portada").attr("src","");
        $("#modaleditar img.portada").attr("src",path+"files/curso/"+data.cur_portada);


        $("#modaleditar textarea[name=requisitos]").val(data.cur_requisitos);
        /*$("#modaleditar .preview").css("background-image","url("+path+"files/cursos/"+data.crs_imagen+")");
        $("#modaleditar .preview img").attr("src",path+"files/cursos/"+data.crs_imagen);*/
        $("#modaleditar").modal("show");


        $("#modaleditar a.eliminar").unbind();
        $("#modaleditar a.eliminar").click(function () {
            $("#modaleditar").modal("hide");
            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {

                new Request("curso/eliminar/" + data.cur_id, null, function (res) {
                    listar(res.cur_estado);
                    $("#modaleliminar").modal("hide");
                });
            });

        })

    });

    return html;
}

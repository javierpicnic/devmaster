$(document).ready(function () {
    console.log(cursoid);
    listar();


    var options = {
        dataType: 'json',
        type: 'post',
        resetForm: true,
        beforeSubmit:function(){
            $("#cargando").fadeIn(200);
            $("#modalagregar .send").prop("disabled", true);
        },
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modalagregar textarea[name=contenido]").val($('#modalagregar .contenido .ql-editor').html());
        },
        success: function (res) {
            if (res.res == "ok") {
                $("#cargando").fadeOut(200);

                $("#modalagregar").modal("hide");
                $("#modalagregar .send").prop("disabled", false);
                $("#planes .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        resetForm: true,
        beforeSubmit:function(){
            $("#cargando").fadeIn(200);
            $("#modaleditar .send").prop("disabled", true);
        },
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modaleditar textarea[name=contenido]").val($('#modaleditar .contenido .ql-editor').html());
        },
        success: function (res) {
            if (res.res == "ok") {
                $("#cargando").fadeOut(200);
                $("#modaleditar").modal("hide");
                $("#modaleditar .send").prop("disabled", false);
                $("#planes .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    $("#planes .lista" ).sortable({
        items: "> tr",
        opacity: 0.7,
        update:function(event,ui){

            $(".orden").show();

        }
    });
    $("#planes .orden .btn-guardar").click(function(){
        var ids = $( "#planes .lista").sortable( "toArray",{attribute:"data-id"} );
        new Request("plan/ordenar",{
            "lista":ids.join(",")
        },function(res){
            listar();
            console.log("orden guardado");
        });
        $("#planes .orden").hide();

    });
    $("#planes .orden .btn-cancelar").click(function(){
        $(".orden").hide();
        $("#planes .lista").empty();
        listar();
    });


//Editor
    $('.edit').each(function(index){
        var container = $('.edit').get(index);

        var options = {
            modules: {
                toolbar:  [
                    ['bold', 'italic', 'underline'],
                    [{'list':'ordered'},{'list':'bullet'}],
                    [{ 'align': [] }],
                    ['link', 'image']
                ],
                table: true
            },
            theme: 'snow'  // or 'bubble'
        }
        new Quill(container,options);
    });

});



function listar() {
    $(".modal input[name=curso]").val(cursoid);
    $("#planes .lista").empty();
    $(".orden").hide();

    var es = $(".filtros select[name=estado]").val();

    new Request("plan/listar/", {
        estado: es,
        curso: cursoid
    }, function (res) {
        console.log(res);
        $("#planes .lista").empty();
        $.each(res, function (k, v) {
            var it = new Itemplan(v);
            $("#planes .lista").append(it);
        })

    });


}



var Itemplan = function (data) {

    var plan_id = data.pln_id;

    var html = $('<tr width="100%" data-id="' + data.pln_id + '">' +
        '<td>' + data.pln_nombre + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-outline-light btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
        '</div></td>' +
        '</tr>');


    html.find(".btn-editar").click(function () {


        $("#modaleditar input[name=id]").val(data.pln_id);
        $("#modaleditar input[name=nombre]").val(data.pln_nombre);
        $('#modaleditar .contenido .ql-editor').html(data.pln_contenido);

        $("#modaleditar").modal("show");

        $("#modaleditar a.eliminar").unbind();
        $("#modaleditar a.eliminar").click(function () {
            $("#modaleditar").modal("hide");
            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {

                new Request("presentacion/eliminar/" + data.prs_id, {
                    producto: producto_id
                }, function (res) {
                    listar(res.prs_estado);
                    $("#modaleliminar").modal("hide");
                });
            });

        })


    });

    return html;

};

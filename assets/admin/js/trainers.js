$(document).ready(function () {
    listar();


    $(".filtros select[name=estado]").bind("change",listar);


    var options = {
        dataType: 'json',
        type: 'post',
        resetForm: true,
        beforeSubmit:function(){
            $("#cargando").fadeIn(200);
            $("#modalagregar .send").prop("disabled", true);
        },
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modalagregar textarea[name=descripcion]").val($('#modalagregar .descripcion .ql-editor').html());
        },
        success: function (res) {
            if (res.res == "ok") {
                $("#cargando").fadeOut(200);

                $("#modalagregar").modal("hide");
                $("#modalagregar .send").prop("disabled", false);
                $("#trainers .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        resetForm: true,
        beforeSubmit:function(){
            $("#cargando").fadeIn(200);
            $("#modaleditar .send").prop("disabled", true);
        },
        beforeSerialize: function ($form, options) {
            // return false to cancel submit
            $("#modaleditar textarea[name=descripcion]").val($('#modaleditar .descripcion .ql-editor').html());
        },
        success: function (res) {
            if (res.res == "ok") {
                $("#cargando").fadeOut(200);
                $("#modaleditar").modal("hide");
                $("#modaleditar .send").prop("disabled", false);
                $("#trainers .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    $("#trainers select[name=estado]").bind("change",function(){
        var es = $(this).val();

        switch (es) {
            case "1":
                $(".lista tr[data-estado='0']").hide();
                $(".lista tr[data-estado='1']").show();
                break;
            case "0":
                $(".lista tr[data-estado='1']").hide();
                $(".lista tr[data-estado='0']").show();
                break;
            default:

        }
    });


//Editor
    $('.edit').each(function(index){
        var container = $('.edit').get(index);

        var options = {
            modules: {
                toolbar:  [
                    ['bold', 'italic', 'underline'],
                    [{'list':'ordered'},{'list':'bullet'}],
                    [{ 'align': [] }],
                    ['link', 'image']
                ],
                table: true
            },
            theme: 'snow'  // or 'bubble'
        }
        new Quill(container,options);
    });

});



function listar() {

    $("#trainers .lista").empty();

    var es = $("#trainers select[name=estado]").val();

    new Request("docente/listar/", {
        'estado': es
    }, function (res) {
        $("#trainers .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemTrainer(v);
            $("#trainers .lista").append(it);
        })

    });


}



var ItemTrainer = function (data) {


    var html = $('<tr width="100%" data-id="' + data.trn_id + '">' +
        '<td>' + data.trn_nombre + '</td>' +
        '<td>' + data.trn_descripcion + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-outline-light btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
        '</div></td>' +
        '</tr>');


    html.find(".btn-editar").click(function () {


        $("#modaleditar input[name=id]").val(data.trn_id);
        $("#modaleditar input[name=nombre]").val(data.trn_nombre);
        $("#modaleditar input[name=linkedin]").val(data.trn_linkedin);
        $("#modaleditar input[name=profesion]").val(data.trn_profesion);
        $('#modaleditar .descripcion .ql-editor').html(data.trn_descripcion);

        $("#modaleditar img.foto").attr("src","");
        $("#modaleditar img.foto").attr("src",path+"files/docente/"+data.trn_foto);

        $("#modaleditar").modal("show");

        $("#modaleditar a.eliminar").unbind();
        $("#modaleditar a.eliminar").click(function () {
            $("#modaleditar").modal("hide");
            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {

                new Request("docente/eliminar/" + data.trn_id, null, function (res) {
                    listar(res.trn_estado);
                    $("#modaleliminar").modal("hide");
                });
            });

        })


    });

    return html;

};

<?php

class Plan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listar($estado, $curso)
    {

        $sql = "SELECT *
                FROM plan
                WHERE pln_estado = ? AND cur_id = ?
                ORDER BY pln_orden ASC";

        $query = $this->db->query($sql, [$estado, $curso]);

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function info($id)
    {
        $query = $this->db->get_where("plan", array("pln_id" => $id));
        if ($query->num_rows() == 0) return null;
        else return $query->first_row();
    }

    public function actualizar($id, $data)
    {

        $this->db->where('pln_id', $id);
        $this->db->update('plan', $data);
        return $this->db->insert_id();
    }


    public function agregar($data)
    {
        $this->db->insert('plan', $data);
        return $this->db->insert_id();
    }

    public function subirorden($ord, $curso)
    {
        $sql = "UPDATE plan SET pln_orden = pln_orden-1 WHERE pln_orden > ? AND cur_id = ?";
        $this->db->query($sql, array($ord, $curso));
    }



}
<?php

class Docente_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listarActivos()
    {
        $sql = "SELECT * FROM trainer  WHERE trn_estado = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function listar($estado)
    {
        $sql = "SELECT * FROM trainer where trn_estado = ?";
        $query = $this->db->query($sql, $estado);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }



    public function agregar($data)
    {
        $this->db->insert('trainer', $data);
        return $this->db->insert_id();
    }


    public function actualizar($id, $data)
    {
        $this->db->where('trn_id', $id);
        $this->db->update('trainer', $data);
        return $this->db->insert_id();
    }

    public function info($id)
    {
        $query = $this->db->get_where("trainer", array("trn_id" => $id));
        if ($query->num_rows() == 0) return null;
        else return $query->first_row();
    }

    public function eliminar($id)
    {
        $this->db->where('trn_id', $id);
        $this->db->delete('trainer');
    }


}
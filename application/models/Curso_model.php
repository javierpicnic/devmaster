<?php

class Curso_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listarCursos()
    {
        $sql = "SELECT cur_id,
                        cur_nombre,
                        cur_fecinicio,
                        cur_duracion_horas,
                        cur_estado,
                        cur_subtitulo,
                        cur_orden,
                        cur_modalidad,
                        cur_descripcion,
                        cur_objetivos,
                        cur_dirigido,
                        cur_requisitos,
                        cur_horario,
                        cur_alias, 
                        cur_short, 
                        cur_foto,
                        trn_nombre,
                        trn_descripcion,
                        trn_alias,
                        trn_profesion,
                        trn_linkedin,
                        trn_foto,
                        mod_id,
                        mod_nombre,
                        trn_nombre,
                        trn_foto
                        FROM curso 
                        INNER JOIN trainer on trainer.trn_id = curso.cur_trainer
                        INNER JOIN modalidad on modalidad.mod_id = curso.cur_modalidad
                        WHERE cur_estado = 1 
                        order by cur_orden ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function listarActivos()
    {
        $sql = "SELECT *
                        FROM curso 
                        WHERE cur_estado = 1 
                        order by cur_orden ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function listar($estado)
    {
        $sql = "SELECT *
                        FROM curso 
                        WHERE cur_estado = ? 
                        order by cur_orden ASC";
        $query = $this->db->query($sql,$estado);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function info($id)
    {
        $query = $this->db->get_where("curso", array("cur_id" => $id));
        if ($query->num_rows() == 0) return null;
        else return $query->first_row();
    }


    public function listarPrincipal()
    {
        $sql = "SELECT *
                        FROM curso 
                        WHERE cur_estado = 1 and cur_principal = 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->first_row();
        } else {
            return FALSE;
        }
    }

    public function principal($id)
    {
        $sql = "UPDATE curso set cur_principal = 0";
        $query = $this->db->query($sql);
        if ($query) {
            $data['cur_principal'] = 1;
            $this->db->where('cur_id', intval($id));
            return $this->db->update('curso', $data);

            //return $query->result();
        } else {
            return FALSE;
        }
    }

    public function infoPorAlias($alias)
    {

        $this->db->select('cur_id,
                        cur_nombre,
                        cur_fecinicio,
                        cur_duracion_horas,
                        cur_estado,
                        cur_subtitulo,
                         cur_modalidad,
                        cur_descripcion,
                        cur_objetivos,
                        cur_dirigido,
                        cur_requisitos,
                        cur_horario,
                        cur_alias, 
                        cur_short, 
                        cur_foto,
                        trn_nombre,
                        trn_descripcion,
                        trn_alias,
                         trn_profesion,
                        trn_linkedin,
                        trn_foto,
                        mod_id,
                        mod_nombre');
        $this->db->from('curso');
        $this->db->join('trainer', 'trainer.trn_id = curso.cur_trainer');
        $this->db->join('modalidad', 'modalidad.mod_id = curso.cur_modalidad');
        $this->db->where('cur_alias', $alias);
        $query = $this->db->get();
        if ($query->num_rows() == 0) return null;
        else return $query->first_row();

    }





    public function agregar($data)
    {
        $this->db->insert('curso', $data);
        return $this->db->insert_id();
    }


    public function actualizar($id, $data)
    {
        $this->db->where('cur_id', $id);
        $this->db->update('curso', $data);
        return $this->db->insert_id();
    }

    public function eliminar($id)
    {
        $this->db->where('cur_id', $id);
        $this->db->delete('curso');
    }

    public function listarpornombres($nombre){

        $sql = "SELECT count(cur_id) as nombres FROM curso where cur_nombre like '%".$nombre."%'";

        $query = $this->db->query($sql);


        if($query->num_rows()>0){
            return $query->first_row();
        }
        else
        {
            return FALSE;
        }
    }

    public function subirorden($ord)
    {
        $sql = "UPDATE curso SET cur_orden = cur_orden-1 WHERE cur_orden > ?";
        $this->db->query($sql, array($ord));
    }

}
<?php

class Modalidad_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function listar(){
        $sql = "SELECT * FROM modalidad";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }


}
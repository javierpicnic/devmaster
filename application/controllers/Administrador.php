<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Administrador extends CI_Controller {


    public function __construct(){
        parent::__construct();

        $this->load->library('email');
    }

    public function login(){

        $usuario = $this->input->post("usuario");
        $clave = $this->input->post("clave");

        if($usuario == "admindev" && $clave == "Devmaster.2019"){

            $res = (object) array(
                "login" => "ok",
                "usuario" => "devadmin"
            );
            $this->session->set_userdata('admin',$res);

            redirect("admin");

        }else{
            redirect("admin/login?error=error");
        }



    }

    public function logout(){
        $this->session->unset_userdata('admin');
        $this->session->sess_destroy();
        redirect("admin/login");
    }


}

<?php

class Curso extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Curso_model", "curso");
        $this->load->library("SimpleImage");
        $this->load->helper('form');
    }

    function listar()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $estado = $this->input->get("estado");
        $lista = $this->curso->listar($estado);
        echo json_encode($lista);
    }

    function listarCursos()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $lista = $this->curso->listarCursos();
        echo json_encode($lista);
    }




    function principal()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $lista = $this->curso->principal($this->input->post("id"));
        echo json_encode($lista);
    }

    function listarPrincipal()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $lista = $this->curso->listarPrincipal();
        echo json_encode($lista);
    }


    function agregar()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        if (
        (isset($_FILES["foto"]) && isset($_FILES["foto"]["tmp_name"]) && $_FILES["foto"]["tmp_name"] != "" && isset($_FILES["portada"]) && isset($_FILES["portada"]["tmp_name"]) && $_FILES["portada"]["tmp_name"] != "")
        ) {

            $nombre = $this->input->post("nombre");
            $subtitulo = $this->input->post("subtitulo");
            $descripcion = $this->input->post("descripcion");
            $short = $this->input->post("short");
            $horario = $this->input->post("horario");
            $fecha = $this->input->post("fecha");
            $modalidad = $this->input->post("modalidad");
            $dirigido = $this->input->post("dirigido");
            $duracion = $this->input->post("duracion");
            $objetivos = $this->input->post("objetivos");
            $requisitos = $this->input->post("requisitos");
            $trainer = $this->input->post("trainer");
            $estado = $this->input->post("estado");


            $alias = $this->alias($nombre);


            $date = date("YmdHis");

            $foto = $date . ".png";
            $temp = $_FILES["foto"]["tmp_name"];

            $img = new SimpleImage($temp);
            $img->best_fit(540, 540); //ancho y alto del contenedor
            $img->save("./files/curso/" . $foto);


            $portada = $date . ".jpg";
            $temp2 = $_FILES["portada"]["tmp_name"];

            $img2 = new SimpleImage($temp2);
            $img2->best_fit(1920, 1080); //ancho y alto del contenedor
            $img2->save("./files/curso/" . $portada);


            $creacion = date("Y-m-d H:i:s");

            $data = array(
                'cur_nombre' => $nombre,
                'cur_alias' => $alias,
                'cur_subtitulo' => $subtitulo,
                'cur_descripcion' => $descripcion,
                'cur_short' => $short,
                'cur_fecinicio' => $fecha,
                'cur_horario' => $horario,
                'cur_modalidad' => $modalidad,
                'cur_dirigido' => $dirigido,
                'cur_duracion_horas' => $duracion,
                'cur_objetivos' => $objetivos,
                'cur_requisitos' => $requisitos,
                'cur_trainer' => $trainer,
                'cur_estado' => $estado,
                'cur_foto' => $foto,
                'cur_portada' => $portada
            );


            if ($estado == 1) {
                $lista = $this->curso->listar(1);
                if ($lista == false) $orden = 1;
                else $orden = count($lista) + 1;
            } else {
                $orden = 0;
            }


            $data["cur_orden"] = $orden;


            $fid = $this->curso->agregar($data);


            $res["res"] = "ok";
            $res["estado"] = $estado;
            echo json_encode($res);
        }
    }

    function actualizar()
    {

        if (!$this->session->has_userdata('admin')) {
            exit();
        }

        $id = $this->input->post("id");
        $nombre = $this->input->post("nombre");
        $subtitulo = $this->input->post("subtitulo");
        $descripcion = $this->input->post("descripcion");
        $short = $this->input->post("short");
        $horario = $this->input->post("horario");
        $modalidad = $this->input->post("modalidad");
        $fecha = $this->input->post("fecha");
        $dirigido = $this->input->post("dirigido");
        $duracion = $this->input->post("duracion");
        $objetivos = $this->input->post("objetivos");
        $requisitos = $this->input->post("requisitos");
        $trainer = $this->input->post("trainer");
        $estado = $this->input->post("estado");

        $alias = $this->slugit($nombre);

        $info = $this->curso->info($id);

        $data = array(
            'cur_nombre' => $nombre,
            'cur_alias' => $alias,
            'cur_subtitulo' => $subtitulo,
            'cur_descripcion' => $descripcion,
            'cur_short' => $short,
            'cur_horario' => $horario,
            'cur_modalidad' => $modalidad,
            'cur_fecinicio' => $fecha,
            'cur_dirigido' => $dirigido,
            'cur_duracion_horas' => $duracion,
            'cur_objetivos' => $objetivos,
            'cur_requisitos' => $requisitos,
            'cur_trainer' => $trainer,
            'cur_estado' => $estado,
        );

        $orden = $info->cur_orden;

        if ($estado == 0) { //Si el nuevo estado es inactivo
            $orden = 0; //Entonces el orden es 0

            if ($info->cur_estado == 1) { //Si el anterior estado era activo

                $this->curso->subirorden($info->cur_orden); //subimos todos los que estaban despues

            }

        } else { //Si el nuevo estado es activo

            if ($info->cur_estado == 0) { //Si el anterior estado era inactivo (osea que hubo cambio)
                $lista = $this->curso->listar(1); //Contamos cuantos activos hay
                if ($lista == false) $orden = 1; //Si esta vacío el nuevo orden es 1
                else $orden = count($lista) + 1; //Si hay varios entonces el nuevo orden es el total+1
            }

        }
        $data["cur_orden"] = $orden;

        $date = date("YmdHis");

        if (isset($_FILES["foto"]) && isset($_FILES["foto"]["tmp_name"]) && $_FILES["foto"]["tmp_name"] != "") {
            if ($info->cur_foto) {
                unlink("./files/curso/" . $info->cur_foto);
            }
            $foto = $date . ".png";

            $temp = $_FILES["foto"]["tmp_name"];

            $img = new SimpleImage($temp);
            $img->best_fit(540, 540); //ancho y alto del contenedor
            $img->save("./files/curso/" . $foto);

            $data["cur_foto"] = $foto;
        }

        if (isset($_FILES["portada"]) && isset($_FILES["portada"]["tmp_name"]) && $_FILES["portada"]["tmp_name"] != "") {
            if ($info->cur_portada) {
                unlink("./files/curso/" . $info->cur_portada);
            }
            $portada = $date . ".jpg";

            $temp = $_FILES["portada"]["tmp_name"];

            $img = new SimpleImage($temp);
            $img->best_fit(1920, 1080); //ancho y alto del contenedor
            $img->save("./files/curso/" . $portada);

            $data["cur_portada"] = $portada;
        }


        $this->curso->actualizar($id, $data);


        $res["res"] = "ok";
        $res["estado"] = $estado;
        echo json_encode($res);

    }

    function eliminar($id)
    {

        if (!$this->session->has_userdata('admin')) {
            exit();
        }

        $info = $this->curso->info($id);


        $this->curso->eliminar($id);

        if ($info->cur_estado == 1) {

            $this->curso->subirorden($info->cur_orden);

        }


        $res["res"] = "ok";
        echo json_encode($res);

    }

    function ordenar()
    {

        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $lista = $this->input->get("lista");

        $lista = explode(",", $lista);


        for ($i = 0; $i < count($lista); $i++) {

            $data = array(
                "cur_orden" => ($i + 1)
            );
            $this->curso->actualizar($lista[$i], $data);
        }


        $res["res"] = "ok";
        echo json_encode($res);
    }

    private function slugit($str, $replace = array(), $delimiter = '-')
    {
        if (!empty($replace)) {
            $str = str_replace((array)$replace, ' ', $str);
        }
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        return $clean;
    }

    private function alias($nombre)
    {

        $total = $this->curso->listarpornombres($nombre);

        $nombres = intval($total->nombres);

        if ($nombres > 0)
            $nombre_foto = $this->slugit($nombre) . ($nombres + 1);
        else
            $nombre_foto = $this->slugit($nombre);


        return $nombre_foto;
    }
}

?>
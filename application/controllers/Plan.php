<?php
class Plan extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model("Plan_model","plan");

    }

    function listar()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $estado = $this->input->get("estado");
        $curso = $this->input->get("curso");

        $lista = $this->plan->listar($estado, $curso);
        echo json_encode($lista);
    }


    function agregar()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }


            $nombre = $this->input->post("nombre");
            $contenido = $this->input->post("contenido");
            $curso = $this->input->post("curso");
            $estado = $this->input->post("estado");


            $data = array(
                'pln_nombre' => $nombre,
                'pln_contenido' => $contenido,
                'cur_id' => $curso,
                'pln_estado' => $estado,

            );


            if ($estado == 1) {
                $lista = $this->plan->listar(1, $curso);
                if ($lista == false) $orden = 1;
                else $orden = count($lista) + 1;
            } else {
                $orden = 0;
            }


            $data["pln_orden"] = $orden;

            $fid = $this->plan->agregar($data);


            $res["res"] = "ok";
            $res["estado"] = $estado;
            echo json_encode($res);

    }



    function actualizar()
    {

        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $id = $this->input->post("id");
        $nombre = $this->input->post("nombre");
        $contenido = $this->input->post("contenido");
        $curso = $this->input->post("curso");
        $estado = $this->input->post("estado");

        $info = $this->plan->info($id);

        $data = array(
            'pln_nombre' => $nombre,
            'pln_contenido' => $contenido,
            'pln_estado' => $estado,
        );

        $orden = $info->pln_orden;

        if ($estado == 0) { //Si el nuevo estado es inactivo
            $orden = 0; //Entonces el orden es 0

            if ($info->pln_estado == 1) { //Si el anterior estado era activo

                $this->plan->subirorden($info->pln_orden, $curso); //subimos todos los que estaban despues

            }

        } else { //Si el nuevo estado es activo

            if ($info->pln_estado == 0) { //Si el anterior estado era inactivo (osea que hubo cambio)
                $lista = $this->plan->listar(1, $curso); //Contamos cuantos activos hay
                if ($lista == false) $orden = 1; //Si esta vacío el nuevo orden es 1
                else $orden = count($lista) + 1; //Si hay varios entonces el nuevo orden es el total+1
            }

        }
        $data["pln_orden"] = $orden;



        $this->plan->actualizar($id, $data);


        $res["res"] = "ok";
        $res["estado"] = $estado;
        echo json_encode($res);

    }
    function ordenar(){

        if(!$this->session->has_userdata('admin')){
            exit();
        }

        $curso = $this->input->get("curso");
        $lista = $this->input->get("lista");

        $lista = explode(",",$lista);


        for($i=0;$i<count($lista);$i++){

            $data = array(
                "pln_orden" => ($i+1)
            );
            $this->plan->actualizar($lista[$i],$data);
        }


        $res["res"] = "ok";
        echo json_encode($res);
    }

}

?>
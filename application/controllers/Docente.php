<?php
class Docente extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model("Docente_model","docente");
        $this->load->library("SimpleImage");
    }

    function listar()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        $estado = $this->input->get("estado");
        $lista = $this->docente->listar($estado);
        echo json_encode($lista);
    }


    function listarActivos()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }

        $lista = $this->docente->listarActivos();
        echo json_encode($lista);
    }



    function agregar()
    {
        if (!$this->session->has_userdata('admin')) {
            exit();
        }
        if (
        (isset($_FILES["foto"]) && isset($_FILES["foto"]["tmp_name"]) && $_FILES["foto"]["tmp_name"] != "")
        ) {

            $nombre = $this->input->post("nombre");
            $profesion = $this->input->post("profesion");
            $descripcion = $this->input->post("descripcion");
            $linkedin = $this->input->post("linkedin");
            $estado = $this->input->post("estado");


            $date = date("YmdHis");

            $foto = $date.".png";
            $temp = $_FILES["foto"]["tmp_name"];

            $img = new SimpleImage($temp);
            $img->best_fit(100,100); //ancho y alto del contenedor
            $img->save("./files/docente/".$foto);


            $creacion = date("Y-m-d H:i:s");

            $data = array(
                'trn_nombre' => $nombre,
                'trn_profesion' => $profesion,
                'trn_descripcion' => $descripcion,
                'trn_linkedin' => $linkedin,
                'trn_estado' => $estado,
                'trn_foto' => $foto
            );


            $fid = $this->docente->agregar($data);


            $res["res"] = "ok";
            $res["estado"] = $estado;
            echo json_encode($res);
        }
    }
    function actualizar()
    {

        if (!$this->session->has_userdata('admin')) {
            exit();
        }

        $id = $this->input->post("id");
        $nombre = $this->input->post("nombre");
        $profesion = $this->input->post("profesion");
        $descripcion = $this->input->post("descripcion");
        $linkedin = $this->input->post("linkedin");
        $estado = $this->input->post("estado");

        $info = $this->docente->info($id);

        $data = array(
            'trn_nombre' => $nombre,
            'trn_profesion' => $profesion,
            'trn_descripcion' => $descripcion,
            'trn_linkedin' => $linkedin,
            'trn_estado' => $estado,
        );


        if ($info->trn_estado == 0) { //Si el anterior estado era inactivo (osea que hubo cambio)
            $lista = $this->docente->listar(1); //Contamos cuantos activos hay
        }

        $date = date("YmdHis");

        if(isset($_FILES["foto"]) && isset($_FILES["foto"]["tmp_name"]) && $_FILES["foto"]["tmp_name"]!=""){
            if($info->trn_foto){
                unlink("./files/docente/".$info->trn_foto);
            }
            $foto = $date.".png";

            $temp = $_FILES["foto"]["tmp_name"];

            $img = new SimpleImage($temp);
            $img->best_fit(100,100); //ancho y alto del contenedor
            $img->save("./files/docente/".$foto);

            $data["trn_foto"] = $foto;
        }


        $this->docente->actualizar($id, $data);


        $res["res"] = "ok";
        $res["estado"] = $estado;
        echo json_encode($res);

    }

    function eliminar($id)
    {

        if (!$this->session->has_userdata('admin')) {
            exit();
        }

        //$info = $this->docente->info($id);


        $this->docente->eliminar($id);

        /*if ($info->trn_estado == 1) {

            $this->docente->subirorden($info->trn_orden);

        }*/


        $res["res"] = "ok";
        echo json_encode($res);

    }

}

?>
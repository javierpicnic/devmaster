<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function index()
    {
        $this->versesion();
        $this->load->view('admin/header');
        $this->load->view('admin/home');
        $this->load->view('admin/footer');

    }


    private function versesion(){

        if(!$this->session->has_userdata('admin')){
            redirect('admin/login');
            exit();
        }
    }

    public function login()
    {
        $this->load->view('admin/login');
    }

    public function cursos(){

        $this->versesion();

        $this->load->view('admin/header');
        $this->load->view('admin/cursos');
        $this->load->view('admin/footer');
    }


    public function banner(){

        $this->versesion();

        $this->load->view('admin/header');
        $this->load->view('admin/banner');
        $this->load->view('admin/footer');
    }

    public function trainers(){

        $this->versesion();

        $this->load->view('admin/header');
        $this->load->view('admin/trainer');
        $this->load->view('admin/footer');
    }

    public function planes($curso){
        $this->versesion();

        $dataView = ["cur_id" => $curso];

        $this->load->view('admin/header');
        $this->load->view('admin/planes', $dataView);
        $this->load->view('admin/footer');
    }

}

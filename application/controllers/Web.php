<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		
		$this->load->model('Curso_model','curso');
        $this->load->model('Plan_model','plan');

	}

	public function index()
	{
        $info = $this->curso->listarPrincipal();
        $cursos = $this->curso->listarActivos();
		$header["titulo"] = "Bienvenidos";
		$header['page'] = 'home';
        $header['class'] = '';

        $data = array(
            "info" => $info,
            "cursos" => $cursos
        );


        $this->load->view('header',$header);
		$this->load->view('home',$data);
		$this->load->view('footer',$header);
	}

	public function nosotros(){
		$header["titulo"] = "Nosotros";
		$header['page'] = 'nosotros';
        $header['class'] = 'bgc';

		$this->load->view('header',$header);
		$this->load->view('nosotros');
		$this->load->view('footer',$header);
	}

    public function contactenos(){
        $header["titulo"] = "Contactenos";
        $header['page'] = 'contactenos';
        $header['class'] = 'bgc';

        $this->load->view('header',$header);
        $this->load->view('contactenos');
        $this->load->view('footer',$header);
    }


	public function cursos(){
		$header["titulo"] = "Cursos";
		$header['page'] = 'cursos';
        $header['class'] = 'bgc';

		$data = array(
			"lista" => $this->curso->listarCursos()
		);

		$this->load->view('header',$header);
		$this->load->view('cursos',$data);
		$this->load->view('footer',$header);
	}



	public function infocurso($alias){


		$info = $this->curso->infoPorAlias($alias);
		$plan = $this->plan->listar(1,$info->cur_id);
        $header['titulo'] = 'Cursos | '.$info->cur_nombre;

		$header['page'] = 'cursodetalle';
		$header['class'] = 'bgc';

		$data = array(
			"info" => $info,
            "plan" => $plan
		);

		$this->load->view('header',$header);
		$this->load->view('cursodetalle',$data);
		$this->load->view('footer',$header);
	}



}

<?php
class Modalidad extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model("Modalidad_model","modalidad");

    }

    function listarmodalidad(){
        if (!$this->session->has_userdata('admin')) {
            exit();
        }

        $lista = $this->modalidad->listar();
        echo json_encode($lista);
    }


}

?>
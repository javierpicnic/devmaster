<script>
    var page = "Cursos";
</script>

<div class="container-fluid breadcrumbs">
    <div class="container">
        Cursos
    </div>
</div>

<div class="container">
    <?php foreach($lista as $it){ ?>

        <div class="curso">

            <div class="curso__data">
                <p class="curso__type">Curso</p>
                <h2 class="curso__title"><?php echo $it->cur_nombre; ?></h2>
                <h3 class="curso__short"><?php echo $it->cur_short; ?></h3>
                <p class="curso__subtitle"><?php echo $it->cur_subtitulo; ?></p>
                <div class="curso__trainer">
                    <img width="30px" height="30px" class="rounded-circle" src="<?php echo base_url()."files/docente/".$it->trn_foto; ?>" alt="">
                    <strong> <?php echo $it->trn_nombre; ?></strong> |
                   <span> <i class="fas fa-clock-o" aria-hidden="true"></i> <?php echo $it->cur_duracion_horas; ?> Hrs.</span>
                </div>
                <a class="curso__button" href="<?php echo base_url()."cursos/".$it->cur_alias; ?>" class="btn principal--button align-self-center align-self-md-start">
                    Ver Curso
                </a>
            </div>

            <img class="curso__image" src="<?php echo base_url()."files/curso/".$it->cur_foto; ?>" alt="">

        </div>


    <?php } ?>
</div>

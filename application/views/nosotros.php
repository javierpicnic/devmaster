

<div class="container-fluid nosotros-banner">

    <div class="container">
        <h1>
            "<strong>Estudia </strong> con los mejores <strong> profesionales</strong>  ahora más <strong>cerca</strong> de ti"
        </h1>
    </div>

</div>

<section class="nosotros-sec">
    <div class="container">
        <div class="nosotros-title">
            Sobre Nosotros
        </div>
        <p>
            Dev Master es un centro de entrenamiento en Tecnología fundado por egresados huachanos de Ia Universidad Nacional de Ingeniería (UNI).
            Nuestra experiencia profesional en Banca, Telecomunicaciones y Consultoría nos respaldan para brindar a la región Lima Provincias los siguientes servicios:
        </p>

        <ul>
            <li>Programas y cursos libres.</li>
            <li>Talleres de preparación para certificaciones internacionales.</li>
            <li>Orientación profesional.</li>
            <li>Capacitación a empresas.</li>
        </ul>

    </div>
</section>

<section class="vision-sec">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-4">
                <div class="nosotros-title red">
                    Misión
                </div>
                <p>Ofrecer un entrenamiento integral y de calidad en Tecnología al público de la región Lima Provincias.</p>
            </div>
            <div class="col-lg-4">
                <div class="nosotros-title red">
                    Visión
                </div>
                <p>
                    Convertirnos en el principal centro de entrenamiento en Tecnología de la región Lima Provincias.
                </p>
            </div>

        </div>
    </div>
</section>

<section class="valores-sec">
    <div class="container">
        <div class="nosotros-title black">
            Valores
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="valor-item">
                    <div class="valor-title">
                        Innovación
                    </div>
                    <div class="valor-content">
                        Buscamos diferenciarnos siempre en todo lo que hacemos
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="valor-item">
                    <div class="valor-title">
                        Compromiso
                    </div>
                    <div class="valor-content">
                        Nuestra palabra es nuestro principal activo ante nuestros alumnos.
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="valor-item">
                    <div class="valor-title">
                        Identidad
                    </div>
                    <div class="valor-content">
                        Comprometidos con nuestras raíces y con el público de la región Lima Provincias.
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="valor-item">
                    <div class="valor-title">
                        Calidad
                    </div>
                    <div class="valor-content">
                        Buscamos la mejora continua en nuestros productos y servicios brindados.
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>


<section class="fotos-sec">
    <div class="container">
        <div class="foto-item">
            <img src="<?php echo base_url(); ?>assets/img/captura1.png">
        </div>
        <div class="foto-item">
            <img src="<?php echo base_url(); ?>assets/img/captura2.png">
        </div>
        <div class="foto-item">
            <img src="<?php echo base_url(); ?>assets/img/captura3.png">
        </div>
    </div>
</section>

<!--
<div class="container info-about">
    <div class="row">
        <div class="col-md-6">
            <h3 class="info--title" id="trainer">

            </h3>

            <div>


            </div>

            <h3 class="info--title" id="trainer">
                Misión
            </h3>

            <h3 class="info--title" id="trainer">
                Visión
            </h3>

            <h3 class="info--title" id="trainer">
                Valores
            </h3>
            <div>

            </div>
        </div>
        <div class="col-md-6 pics popup-gallery">
            <a title="Exterior Devmaster" href="<?php echo base_url(); ?>assets/img/captura1.png">

            </a>
            <a title="Hall de descanso" href="<?php echo base_url(); ?>assets/img/captura2.png">
            <img src="<?php echo base_url(); ?>assets/img/captura2.png">
            </a>
            <a title="Laboratorio" href="<?php echo base_url(); ?>assets/img/captura3.png">
            <img src="<?php echo base_url(); ?>assets/img/captura3.png">
            </a>

        </div>
    </div>
</div>
-->



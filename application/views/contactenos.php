<script>
    var page = 'Contáctenos';
</script>
<div class="container-fluid breadcrumbs">
    <div class="container">
        Contáctenos
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="padding: 0">
            <div id="map" class="contact" style=" height: 400px"></div>
            <script>
                function initMap() {
                    var uluru = {lat: -11.120998, lng: -77.595012};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 15,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                        scaleControl: false,
                        center: {lat: -11.120998, lng:  -77.595012},
                        styles:[
                            {
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "elementType": "labels.text.stroke",
                                "stylers": [
                                    {
                                        "color": "#f5f5f5"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative.land_parcel",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#86e586"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi.park",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#ffffff"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#757575"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#dadada"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    },
                                    {
                                        "weight": 6.5
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text",
                                "stylers": [
                                    {
                                        "saturation": 100
                                    },
                                    {
                                        "lightness": 70
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#616161"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.local",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.line",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#e5e5e5"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit.station",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#eeeeee"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry",
                                "stylers": [
                                    {
                                        "color": "#c9c9c9"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "geometry.fill",
                                "stylers": [
                                    {
                                        "color": "#68b3eb"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#9e9e9e"
                                    }
                                ]
                            }
                        ],
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_CENTER
                        }
                    });
                    var marker = new google.maps.Marker({
                        position: uluru,
                        map: map
                    });
                }
            </script>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT3HBxjIqIR83aYkVKq2ftyfuTCJDoBRc&callback=initMap">
            </script>

        </div>


    </div>



</div>
<div class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-md-6">
            <div class="contacto-data ">
                <ul class="d-flex flex-column justify-content-center align-items-center">
                    <li><i class="fas fa-map-marker-alt" aria-hidden="true"></i> M24 - Urb. Los Cipreses - Huacho</li>
                    <li><i class="far fa-envelope" aria-hidden="true"></i> info@devmaster.pe</li>
                    <li><i class="fas fa-phone" aria-hidden="true"></i> 966 163 471</li>
                </ul>

            </div>
        </div>
        <div class="col-md-6">

            <h1 class="contactForm2--title">Contáctenos</h1>


            <div class="ws" style="margin-bottom: 30px">
                <a href="https://api.whatsapp.com/send?phone=51995995177&text=Necesito%20información"
                   target="_blank"><i class="fab fa-whatsapp"></i> Whatsapp Chat</a>
            </div>
        </div>
    </div>
</div>



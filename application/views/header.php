<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <title><?php echo $titulo; ?> | Devmaster</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/animate.min.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-dev.css"-->
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>

<script>
    var path = '<?php echo base_url(); ?>';
</script>


<a href="#" target="_blank" class="whatsapp-button">
    <i class="fab fa-whatsapp"></i>
</a>

<div class="ws-chat">
    <div class="wrap">
        <div class="top">
            <div class="close-link"><i class="fa fa-times"></i></div>
            <img src="<?php echo base_url(); ?>assets/img/logo_favicon.svg" alt="">
            <div class="info-text">
                <strong>Devmaster</strong>
                <span>En línea</span>
            </div>
        </div>
        <div class="bottom">
            <textarea id="mensaje" name="" cols="30" rows="1" placeholder="Escribe un mensaje aqui"></textarea>
            <!--<input type="text" id="mensaje" placeholder="Escribe un mensaje aqui">-->
            <div class="send" data-tel="51995995177">
                <img width="25px" src="<?php echo base_url(); ?>assets/img/send.svg" alt="">
            </div>
        </div>
    </div>
</div>


<div class="nav-top <?php echo $class; ?>">
    <div class="container d-flex justify-content-between align-items-center">
        <a href="<?php echo base_url(); ?>"><img class="logotipo" src="<?php echo base_url(); ?>assets/img/logo_18_blanco.svg" alt=""></a>
        <div class="close-button">
            <span></span>
            <span></span>
        </div>

    </div>
</div>

<div class="menu-fullscreen">
    <nav class="menu">
        <ul class="menu-principal">
            <li><a href="<?php echo base_url(); ?>nosotros">Nosotros</a></li>
            <li><a href="<?php echo base_url(); ?>cursos">Cursos</a></li>
        </ul>
    </nav>

</div>


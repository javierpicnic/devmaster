<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>Admin | Devmaster </title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/bootstrap/bootstrap.min.css">
	<!--<link rel="stylesheet" href="bootstrap/css/bootstrap-flaty.min.css">-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/libs/jquery-ui.min.css">
	<script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.2.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/libs/bootstrap/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bootstrap/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/libs/jquery.form.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery.app.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
  <style>

      #cargando{
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0px;
        display: none;
        left: 0px;
        background-color: rgba(0,0,0,0.7);
        z-index: 9999;
      }
      #cargando .table{
        width: 100%;
        height: 100%;
        display: table;
      }
      #cargando .table .cell{
        width: 100%;
        height: 100%;
        display: table-cell;
        vertical-align: middle;
      }
      .spinner {
        width: 40px;
        height: 40px;

        position: relative;
        margin: 100px auto;
      }

      .double-bounce1, .double-bounce2 {
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background-color: #fff;
        opacity: 0.6;
        position: absolute;
        top: 0;
        left: 0;

        -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
        animation: sk-bounce 2.0s infinite ease-in-out;
      }

      .double-bounce2 {
        -webkit-animation-delay: -1.0s;
        animation-delay: -1.0s;
      }

      @-webkit-keyframes sk-bounce {
        0%, 100% { -webkit-transform: scale(0.0) }
        50% { -webkit-transform: scale(1.0) }
      }

      @keyframes sk-bounce {
        0%, 100% {
          transform: scale(0.0);
          -webkit-transform: scale(0.0);
        } 50% {
          transform: scale(1.0);
          -webkit-transform: scale(1.0);
        }
      }

  </style>

  <script>
  var path = '<?php echo base_url(); ?>';
  </script>

  <script src="<?php echo base_url(); ?>assets/admin/js/main.js?v=2"></script>


</head>
<body>
<div class="modal" tabindex="-1" role="dialog" id="modalalerta">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<div id="cargando">
  <div class="table">
    <div class="cell">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>
  </div>

</div>






<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">
                <a href="<?php echo base_url(); ?>admin" class="logo">
                    <img src="<?php echo base_url(); ?>assets/img/logo_18_blanco.svg" alt="" height="30">
                </a>
            </div>
            <!-- End Logo container-->


            <div class="menu-extras topbar-custom">

                <ul class="list-unstyled topbar-right-menu float-right mb-0">

                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>


                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="<?php echo base_url(); ?>assets/admin/img/avatar-1.jpg" alt="user" class="rounded-circle">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown">

                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item btn-logout">
                                <i class="ti-power-off m-r-5"></i> Logout
                            </a>

                        </div>
                    </li>

                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li class="has-submenu">
                        <a href="<?php echo base_url(); ?>admin"><span> Dashboard </span> </a>
                    </li>
                    <li class="has-submenu">
                        <a href="<?php echo base_url(); ?>admin/trainers"><span> Trainers </span> </a>
                    </li>
                    <li class="has-submenu">
                        <a href="<?php echo base_url(); ?>admin/cursos"><span> Cursos </span> </a>
                    </li>

                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>

<div class="wrapper">
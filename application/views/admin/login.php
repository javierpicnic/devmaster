<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<title>Menorca</title>

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/libs/bootstrap/bootstrap.min.css">
	<!--<link rel="stylesheet" href="bootstrap/css/bootstrap-flaty.min.css">-->

	<script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bootstrap/popper.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/libs/jquery.form.js"></script>

	<script>
	var path = '<?php echo base_url(); ?>';
	</script>
	<style>
	form{
		width: 400px;

	}
	input{
		margin: 20px 0;
        display: block;
        border: none;
        width: 100%;
        padding: 20px;
        border-bottom: solid 1px #000;
        font-size: 18px;
        overflow: hidden;
	}
    input:focus{
        outline: none;
    }
	.btn-danger{
		margin: auto;
		display: block;
		width: 100%;
        border-radius: 50px;
        padding: 20px;
        text-transform: uppercase;
        font-weight: bold;
        box-shadow: 0 10px 15px -3px rgba(216, 24, 42, 0.25);
	}

    html,body{
        height: 100%;
    }
    #login{
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 100%;
    }
    .logo{
        width: 100%;
        max-width: 350px;
    }

        @media screen and (max-width: 768px){
            #login{
                flex-direction: column;
                justify-content: center;
            }
        }
    @media screen and (max-width: 992px){
        .logo{
            max-width: 250px;
        }
    }
	</style>
</head>
<body>


<div class="container" id="login">
    <img src="<?php echo base_url() ?>assets/img/logo_18_color.svg" alt="" class="logo">

    <form class="form-signin" role="form" action="<?php echo base_url() ?>administrador/login" method="post">
        <br>

        <input type="text" class="inuser" placeholder="Usuario" name="usuario" required autofocus>
        <input type="password" class="inclave" placeholder="Clave" name="clave" required>

        <button class="btn btn-danger" type="submit">Entrar</button>
        <br><br>
        <?php if(isset($_GET["error"])){ ?>
            <div class="alert alert-danger">¡Usuario y/o clave incorrectos!</div>
        <?php } ?>
    </form>

</div>







</body>
</html>

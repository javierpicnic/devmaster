<script src="https://cdn.quilljs.com/2.0.0-dev.2/quill.js"></script>
<link rel="stylesheet" href="https://cdn.quilljs.com/2.0.0-dev.2/quill.bubble.css">

<link rel="stylesheet" href="https://cdn.quilljs.com/2.0.0-dev.2/quill.snow.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/cursos.css">
<script src="<?php echo base_url(); ?>assets/admin/js/cursos.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>


<div class="modal" tabindex="-1" role="dialog" id="modalagregar">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url(); ?>curso/agregar" method="post">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Subtitulo</label>
                        <input type="text" name="subtitulo" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Descripción corta</label>
                        <input type="text" name="short" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Fecha de Inicio</label>
                        <input type="text" name="fecha" class="datepicker form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Duracion</label>
                        <input type="number" name="duracion" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Horario</label>
                        <input type="text" name="horario" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea name="descripcion" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Objetivos</label>
                        <div class="edit objetivos-t"></div>
                        <textarea name="objetivos" style="display:none"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Dirigido</label>
                        <textarea name="dirigido" class="form-control" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Requisitos</label>
                        <textarea name="requisitos" class="form-control" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Modalidad</label>
                        <select name="modalidad" class="form-control" id="">

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Trainer</label>
                        <select name="trainer" class="form-control" id="">

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Foto medida(540x540px)</label>
                        <input type="file" name="foto" accept=".png" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Portada medida(1920x1080px)</label>
                        <input type="file" name="portada" accept=".jpg" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Estado</label>
                        <select name="estado" class="form-control" required>
                            <option value="1" selected>Habilitado</option>
                            <option value="0">Deshabilitado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary send">Agregar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modaleditar">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title">Editar Curso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url(); ?>curso/actualizar" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Id</label>
                        <input type="text" name="id" class="form-control" required readonly>
                    </div>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Subtitulo</label>
                        <input type="text" name="subtitulo" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Descripción corta</label>
                        <input type="text" name="short" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Fecha de Inicio</label>
                        <input type="text" name="fecha" class="datepicker form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Duracion</label>
                        <input type="number" name="duracion" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Horario</label>
                        <input type="text" name="horario" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea name="descripcion" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Objetivos</label>
                        <div class="edit objetivos-t"></div>
                        <textarea name="objetivos" style="display:none"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Dirigido</label>
                        <textarea name="dirigido" class="form-control" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Requisitos</label>
                        <textarea name="requisitos" class="form-control" rows="2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Modalidad</label>
                        <select name="modalidad" class="form-control" id="">

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Trainer</label>
                        <select name="trainer" class="form-control" id="">

                        </select>
                    </div>
                    <div class="form-group">
                        <label>Foto Actual</label>
                        <img src="" class="foto" alt="" style="display: block;width: 100%;max-width: 350px">
                    </div>

                    <div class="form-group">
                        <label>Foto medida(540x540px)</label>
                        <input type="file" name="foto" accept=".png" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Portada Actual</label>
                        <img src="" class="portada" alt="" style="display: block;width: 100%;max-width: 350px">
                    </div>

                    <div class="form-group">
                        <label>Portada medida(1920x1080px)</label>
                        <input type="file" name="portada" accept=".jpg" class="form-control">
                    </div>



                    <div class="form-group">
                        <label>Estado</label>
                        <select name="estado" class="form-control" required>
                            <option value="1" selected>Habilitado</option>
                            <option value="0">Deshabilitado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="eliminar" href="#modaleliminar" data-toggle="modal" style="position:absolute; left:15px; color:red">Eliminar</a>
                    <button type="submit" class="btn btn-primary send">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">¡Alerta!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Está seguro que desea eliminar este curso?, esta acción no se puede revertir</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-eliminar">Eliminar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <h1 class="page-title">Cursos</h1>


    <a href="<?php echo base_url(); ?>admin/cursos/agregar" class="btn btn-success btn-agregar mb-5"><i
                class="fas fa-plus-circle"></i> Agregar</a>

    <br>
    <div class="row filtros">
        <div class="col-sm-4">
            <label>Estado </label>
            <select class="form-control" name="estado">

                <option value="0">Desactivado</option>
                <option value="1" selected>Activado</option>
            </select>
        </div>
        <div class="col-sm-8 orden">
            <label>Cambios de orden</label><br>
            <button class="btn btn-primary btn-guardar">Guardar</button>
            <button class="btn btn btn-outline-secondary btn-cancelar">Deshacer</button>
        </div>

    </div>
    <br>

    <div class="row" id="cursos">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nombre</th>
                <th width="400">Acciones</th>
                <th>Principal</th>
            </tr>
            </thead>
            <tbody class="lista">

            </tbody>
        </table>

    </div>

</div>

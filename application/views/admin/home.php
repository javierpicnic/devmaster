<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/home.css?v=1.4">




<div class="container">
    <h1 class="page-title">Dashboard</h1>
    <div class="row">
        <div class="col-sm-3">
                <a class="item" href="<?php echo base_url(); ?>admin/cursos">
                    <div class="icono">
                    <i class="fas fa-book icono"></i>
                    </div>
                    <div class="nom">Cursos</div>
                </a>
        </div>
        <div class="col-sm-3">
            <a class="item" href="<?php echo base_url(); ?>admin/trainers">
                <div class="icono">
                    <i class="fas fa-user icono"></i>
                </div>
                <div class="nom">Trainers</div>
            </a>
        </div>

    </div>
</div>

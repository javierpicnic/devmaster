<script src="https://cdn.quilljs.com/2.0.0-dev.2/quill.js"></script>
<link rel="stylesheet" href="https://cdn.quilljs.com/2.0.0-dev.2/quill.bubble.css">

<link rel="stylesheet" href="https://cdn.quilljs.com/2.0.0-dev.2/quill.snow.css">

<script src="<?php echo base_url(); ?>assets/admin/js/trainers.js"></script>


<div class="modal" tabindex="-1" role="dialog" id="modalagregar">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Trainer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url(); ?>docente/agregar" method="post">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Profesion</label>
                        <input type="text" name="profesion" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <div class="edit descripcion"></div>
                        <textarea rows="6" name="descripcion" class="form-control" style="display:none"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Linkedin</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">www.linkedin.com/in/</div>
                            </div>
                            <input type="text" name="linkedin" id="inlineFormInputGroupUsername" class="form-control" required>
                        </div>

                    </div>
                    <div class="form-group">
                        <label>Foto Actual</label>
                        <img src="" class="foto" alt="" style="display: block;width: 100%;max-width: 350px">
                    </div>

                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto" accept=".png" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Estado</label>
                        <select name="estado" class="form-control" required>
                            <option value="1" selected>Habilitado</option>
                            <option value="0">Deshabilitado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary send">Agregar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modaleditar">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title">Editar Trainer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url(); ?>docente/actualizar" method="post">
                <div class="modal-body">
                    <div class="form-group" hidden>
                        <label>ID</label>
                        <input type="text" name="id" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="nombre" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Profesion</label>
                        <input type="text" name="profesion" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Descripción</label>
                        <div class="edit descripcion"></div>
                        <textarea rows="6" name="descripcion" class="form-control" style="display:none"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Linkedin</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">www.linkedin.com/in/</div>
                            </div>
                            <input type="text" name="linkedin" id="inlineFormInputGroupUsername" class="form-control" required>
                        </div>

                    </div>
                    <div class="form-group">
                        <label>Foto Actual</label>
                        <img src="" class="foto" alt="" style="display: block;width: 100%;max-width: 100px">
                    </div>

                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto" accept=".png" class="form-control">
                    </div>


                    <div class="form-group">
                        <label>Estado</label>
                        <select name="estado" class="form-control" required>
                            <option value="1" selected>Habilitado</option>
                            <option value="0">Deshabilitado</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="eliminar" href="#modaleliminar" data-toggle="modal" style="position:absolute; left:15px; color:red">Eliminar</a>
                    <button type="submit" class="btn btn-primary send">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">¡Alerta!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>¿Está seguro que desea eliminar este profesor?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-eliminar">Eliminar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>


<div class="container" id="trainers">

    <h4 class="page-title">Trainers</h4>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar"><i class="fas fa-plus-circle"></i> Agregar</button>

            </div>
        </div>
    </div>


    <div class="row filtros">
        <div class="col-sm-4">
            <div class="card-box">
                <label>Estado</label>
                <select class="form-control" name="estado">
                    <option value="1" selected>Habilitados</option>
                    <option value="0">Desactivados</option>
                </select>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
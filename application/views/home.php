
<?php

function get_month_name($month)
{
    $months = array(
        "01"   =>  'Enero',
        "02"   =>  'Febrero',
        "03"   =>  'Marzo',
        "04"   =>  'Abril',
        "05"   =>  'Mayo',
        "06"   =>  'Junio',
        "07"   =>  'Julio',
        "08"   =>  'Agosto',
        "09"   =>  'Septtiembre',
        "10"  =>  'Octubre',
        "11"  =>  'Noviembre',
        "12"  =>  'Diciembre'
    );

    return $months[$month];
} ;?>


<section class="container-fluid banner-principal" style="background-image: url(<?php echo base_url(); ?>files/curso/<?php echo $info->cur_portada; ?>)">

    <div class="block-red"></div>
    <div class="block-blue"></div>


    <div class="container curso-principal">
        <p class="animated fadeInUp">curso</p>
        <h1 class="animated fadeInUp"><?php echo $info->cur_nombre; ?></h1>
        <h2 class="animated fadeInUp"><?php echo $info->cur_subtitulo; ?></h2>
        <a href="<?php echo base_url(); ?>cursos/<?php echo $info->cur_alias; ?>" class="btn btn-outline-light animated fadeInUp btn-devmaster-light">VER MÁS</a>
    </div>

    <a href="#nosotros" class="goto">
        <i class="fas fa-chevron-down"></i>
    </a>




</section>

<section class="container-fluid nosotros-section" id="nosotros">
    <div class="container">
        <div class="titulo"  data-aos="fade-up">Nosotros</div>
        <div class="row align-items-center"  data-aos="fade-up" data-aos-delay="300">
            <div class="col-lg-6">
                <p>
                    Dev Master es un centro de entrenamiento en Tecnología fundado por egresados huachanos de Ia
                    Universidad Nacional de Ingeniería (UNI). Nuestra experiencia profesional en Banca,
                    Telecomunicaciones y Consultoría nos respaldan.
                </p>
                <a href="<?php echo base_url(); ?>nosotros" class="btn btn-devmaster">Conocer más</a>

            </div>
            <div class="col-lg-6">
                <img width="100%" style="border-radius: 5px;" src="<?php echo base_url(); ?>assets/img/frontis.jpg" alt="">
            </div>
        </div>
    </div>

</section>



<section class="cursos-section">




    <div class="container">
        <div class="titulo light" data-aos="fade-up" data-aos-delay="0">Cursos</div>

       <div class="row cards">


           <?php foreach($cursos as $key=>$value): ?>

               <?php
                    $delay =  300*$key;

               ?>

               <div class="col-xl-4 col-lg-6 col-sm-6" data-aos="fade-up" data-aos-delay="<?php echo $delay?>">
                   <article class="card card--1">
                       <div class="card__info-hover">
                           <span>Inicio</span>
                           <strong><?php $fecha = $value->cur_fecinicio; $fechas = explode("/", $fecha); echo $fechas[1] ; ?>
                               <?php $fecha = $value->cur_fecinicio; $fechas = explode("/", $fecha); echo get_month_name("$fechas[0]"); ?></strong>
                       </div>
                       <div class="card__img" style="background-image: url('<?php echo base_url(); ?>files/curso/<?php echo $value->cur_foto?>')"></div>
                       <a href="<?php echo base_url(); ?>cursos/<?php echo $value->cur_alias?>" class="card_link">
                           <div class="card__img--hover" style="background-image: url('<?php echo base_url(); ?>files/curso/<?php echo $value->cur_foto?>')"></div>
                       </a>
                       <div class="card__info">
                           <span class="card__category"> Curso</span>
                           <h3 class="card__title"><?php echo $value->cur_nombre?></h3>
                           <span class="card__by"><?php echo $value->cur_subtitulo?></span>
                       </div>
                   </article>
               </div>



           <?php endforeach; ?>




       </div>
    </div>

</section>

<section class="contacto">


    <div id="map"></div>

    <div class="dir">
        <i class="fas fa-map-pin"></i> Mz. M-24 - Urb. Los Cipreses - Huacho
    </div>
</section>


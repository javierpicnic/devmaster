



<footer class="footer">

    <div class="container">

        <div class="sociales">
            <a href="https://www.facebook.com/devmasterperu/" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="http://m.me/devmasterperu" target="_blank"><i class="fab fa-facebook-messenger"></i></a>
            <a href="mailto:gmanriquev@devmaster.pe" target="_blank"><i class="far fa-envelope"></i></a>
        </div>
       <p>
           Devmaster 2019 | Todos los derechos reservados &copy;
       </p>
    </div>


</footer>



<script src="<?php echo base_url(); ?>assets/libs/jquery-3.3.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/libs/bootstrap/popper.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/libs/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/libs/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/libs/imagesloaded.pkgd.min.js" type="text/javascript"></script>


<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/js/<?php echo $page; ?>.js"></script>



<?php if ($page == "home") : ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsOzBNpYWTP13pZ2k5fD1bXIEJ3zMdqic&callback=initMap"
        async defer></script>
<?php endif ?>
</body>
</html>

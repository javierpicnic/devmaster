<script>
    var page = '<?php echo $info->cur_nombre; ?>';

</script>

<?php

function get_month_name($month)
{
    $months = array(
        "01"   =>  'Ene.',
        "02"   =>  'Feb.',
        "03"   =>  'Mar.',
        "04"   =>  'Abr.',
        "05"   =>  'May.',
        "06"   =>  'Jun.',
        "07"   =>  'Jul.',
        "08"   =>  'Ago.',
        "09"   =>  'Sept.',
        "10"  =>  'Oct.',
        "11"  =>  'Nov.',
        "12"  =>  'Dic.'
    );

    return $months[$month];
} ;?>


<section class="curso-top">
    <div class="container">
      <div class="row align-items-center flex-column-reverse flex-lg-row">
          <div class="col-xl-6 col-lg-6">
              <div class="curso-tipo">Curso</div>
              <h1 class="curso-title"><?php echo $info->cur_nombre; ?></h1>

              <p class="curso-subtitle"><?php echo $info->cur_subtitulo; ?></p>
              <span class="curso-short"><?php echo $info->cur_short; ?></span>

              <div class="inicio">
                  <span>Inicio</span>
                  <div class="num"><?php $fecha = $info->cur_fecinicio; $fechas = explode("/", $fecha); echo $fechas[1] ; ?></div>
                  <div class="mes"><?php $fecha = $info->cur_fecinicio; $fechas = explode("/", $fecha); echo get_month_name("$fechas[0]"); ?></div>
              </div>
          </div>
          <div class="col-xl-6 col-lg-6">
              <img width="100%" src="<?php echo base_url(); ?>files/curso/<?php echo $info->cur_foto; ?>" alt="">
          </div>
      </div>
    </div>
    
</section>

<section class="info">
    <div class="container">
        <div class="row">
            <div class="col-xl-7">

                <strong class="subtitle">
                    Información General
                </strong>

                <div class="info-general">
                    <div class="item-general">
                        <i class="far fa-clock" aria-hidden="true"></i>
                        <p><strong>Duración: </strong><?php echo $info->cur_duracion_horas; ?> Hrs.</p>
                    </div>
                    <div class="item-general">
                        <i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                        <p><strong>Modalidad: </strong><?php echo $info->mod_nombre; ?></p>
                    </div>

                    <div class="item-general">
                        <i class="far fa-calendar-alt" aria-hidden="true"></i>
                        <p><strong>Horario: </strong> <?php echo $info->cur_horario; ?></p>
                    </div>

                </div>

                <strong class="subtitle">
                    Descripción
                </strong>
                <p>
                    <?php echo $info->cur_descripcion; ?>
                </p>


                <strong class="subtitle">
                    Objetivos
                </strong>
                <p>
                    <?php echo $info->cur_objetivos; ?>
                </p>


                <strong class="subtitle">
                    Dirigido a
                </strong>
                <p>
                    <?php echo $info->cur_dirigido; ?>

                </p>


                <strong class="subtitle">
                    Requisitos
                </strong>
                <p>
                    <?php echo $info->cur_requisitos; ?>
                </p>


                <strong class="subtitle">
                    Plan de Estudios
                </strong>

                <div class="acordion-box">
                    <div class="accordion" id="accordionPlan">

                        <?php if($plan != false): ?>

                        <?php foreach($plan as $key=>$value): ?>



                            <div class="card">
                                <div class="card-header" id="planes<?php echo $value->pln_id?>">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#plan<?php echo $value->pln_id?>" aria-expanded="false" aria-controls="collapse<?php echo $value->pln_id?>" tabindex="0">
                                            <span><?php echo $value->pln_nombre?></span>
                                            <i class="fas fa-chevron-up"></i>
                                        </button>
                                    </h2>
                                </div>

                                <div id="plan<?php echo $value->pln_id?>" class="collapse" aria-labelledby="heading<?php echo $value->pln_id?>" data-parent="#accordionPlan">
                                    <div class="card-body">
                                        <?php echo $value->pln_contenido?>
                                    </div>
                                </div>
                            </div>


                        <?php endforeach; ?>

                        <?php endif ?>


                    </div>

                    <div class="more">Ver más</div>
                </div>

                <strong class="subtitle">
                    Dev Trainer
                </strong>

                <div class="docente">

                    <div class="dev-pic">
                        <div class="docente-img">
                            <img width="100%"
                                 src="<?php echo base_url(); ?>/files/docente/<?php echo $info->trn_foto; ?>"
                                 alt="<?php echo $info->trn_alias; ?>">

                        </div>
                        <a href="http://linkedin.com/in/<?php echo $info->trn_linkedin; ?>" class="social" target="_blank">Linked <i
                                    class="fab fa-linkedin" aria-hidden="true"></i></a>
                    </div>

                    <div>
                        <h4><?php echo $info->trn_nombre; ?></h4>
                        <strong><?php echo $info->trn_profesion; ?></strong>
                        <p>
                            <?php echo $info->trn_descripcion; ?>
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-xl-5">
                <strong class="subtitle">
                    ¿Tienes preguntas?, Escríbenos
                </strong>

                <div class="ws">
                    <a href="https://api.whatsapp.com/send?phone=51995995177&text=Estoy%20interesado%20en%20el%20curso%20de%20<?php echo $info->cur_nombre; ?>"
                       target="_blank"><i class="fab fa-whatsapp"></i><span> Whatsapp Chat</span></a>
                </div>

                <strong class="subtitle">
                    Beneficios
                </strong>

                <div class="beneficios">
                    <div class="beneficio-item">
                        <img src="<?php echo base_url(); ?>assets/img/coffe-break.svg" alt="Coffee break"> <span>Coffe Break</span>
                    </div>
                    <div class="beneficio-item">
                        <img src="<?php echo base_url(); ?>assets/img/teacher.svg" alt="Mentoria"> <span>Mentoria</span>
                    </div>
                    <div class="beneficio-item">
                        <img src="<?php echo base_url(); ?>assets/img/student.svg" alt="Clases Presenciales"> <span>Clases
                        Presenciales</span>
                    </div>
                    <div class="beneficio-item">
                        <img src="<?php echo base_url(); ?>assets/img/pdf.svg" alt="Material PDF"> <span>Material PDF</span>
                    </div>
                    <div class="beneficio-item">
                        <img src="<?php echo base_url(); ?>assets/img/talleres.svg" alt="Talleres"> <span> Talleres</span>
                    </div>
                    <div class="beneficio-item">
                        <img src="<?php echo base_url(); ?>assets/img/diploma.svg" alt="Diploma de Participación">
                       <span> Diploma de Participación</span>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>


